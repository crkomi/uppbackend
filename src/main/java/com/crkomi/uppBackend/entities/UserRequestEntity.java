package com.crkomi.uppBackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class UserRequestEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String typeJob;
    private String descriptionJob;
    private long maxValue;
    private Date lastDateReceiptTenders;
    private long maxOffers;
    private Date lastDateCompletionJob;
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "userRequestEntity")
    private List<OfferEntity> offerEntities = new ArrayList<>();

    public UserRequestEntity(){

    }

    public UserRequestEntity(String typeJob, String descriptionJob, long maxValue, Date lastDateReceiptTenders, long maxOffers, Date lastDateCompletionJob, List<OfferEntity> offerEntities) {
        this.typeJob = typeJob;
        this.descriptionJob = descriptionJob;
        this.maxValue = maxValue;
        this.lastDateReceiptTenders = lastDateReceiptTenders;
        this.maxOffers = maxOffers;
        this.lastDateCompletionJob = lastDateCompletionJob;
        this.offerEntities = offerEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeJob() {
        return typeJob;
    }

    public void setTypeJob(String typeJob) {
        this.typeJob = typeJob;
    }

    public String getDescriptionJob() {
        return descriptionJob;
    }

    public void setDescriptionJob(String descriptionJob) {
        this.descriptionJob = descriptionJob;
    }

    public long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(long maxValue) {
        this.maxValue = maxValue;
    }

    public Date getLastDateReceiptTenders() {
        return lastDateReceiptTenders;
    }

    public void setLastDateReceiptTenders(Date lastDateReceiptTenders) {
        this.lastDateReceiptTenders = lastDateReceiptTenders;
    }

    public long getMaxOffers() {
        return maxOffers;
    }

    public void setMaxOffers(long maxOffers) {
        this.maxOffers = maxOffers;
    }

    public Date getLastDateCompletionJob() {
        return lastDateCompletionJob;
    }

    public void setLastDateCompletionJob(Date lastDateCompletionJob) {
        this.lastDateCompletionJob = lastDateCompletionJob;
    }

    public List<OfferEntity> getOfferEntities() {
        return offerEntities;
    }

    public void setOfferEntities(List<OfferEntity> offerEntities) {
        this.offerEntities = offerEntities;
    }
}
