package com.crkomi.uppBackend.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class OfferEntity {

    @Id
    @GeneratedValue
    private Long id;
    private Date dateLimit;
    private long price;
    private Long userId;
    private String username;
    private String state;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private UserRequestEntity userRequestEntity;

    public OfferEntity(){

    }

    public OfferEntity(Date dateLimit, long price, Long userId, String state, UserRequestEntity userRequestEntity, String username) {
        this.dateLimit = dateLimit;
        this.price = price;
        this.userId = userId;
        this.state = state;
        this.userRequestEntity = userRequestEntity;
        this.username = username;
    }

    public Date getDateLimit() {
        return dateLimit;
    }

    public void setDateLimit(Date dateLimit) {
        this.dateLimit = dateLimit;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRequestEntity getUserRequestEntity() {
        return userRequestEntity;
    }

    public void setUserRequestEntity(UserRequestEntity userRequestEntity) {
        this.userRequestEntity = userRequestEntity;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
