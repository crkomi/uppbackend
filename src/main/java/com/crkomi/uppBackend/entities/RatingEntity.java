package com.crkomi.uppBackend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class RatingEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String usernameOne;
    private String usernameTwo;
    private Long ration;

    public RatingEntity(){

    }

    public RatingEntity(String usernameOne, String usernameTwo, Long ration) {
        this.usernameOne = usernameOne;
        this.usernameTwo = usernameTwo;
        this.ration = ration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsernameOne() {
        return usernameOne;
    }

    public void setUsernameOne(String usernameOne) {
        this.usernameOne = usernameOne;
    }

    public String getUsernameTwo() {
        return usernameTwo;
    }

    public void setUsernameTwo(String usernameTwo) {
        this.usernameTwo = usernameTwo;
    }

    public Long getRation() {
        return ration;
    }

    public void setRation(Long ration) {
        this.ration = ration;
    }
}
