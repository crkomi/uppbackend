package com.crkomi.uppBackend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CompanyCategoryEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String activitiName;

    public CompanyCategoryEntity(String name, String activitiName) {
        this.name = name;
        this.activitiName = activitiName;
    }

    public CompanyCategoryEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivitiName() {
        return activitiName;
    }

    public void setActivitiName(String activitiName) {
        this.activitiName = activitiName;
    }
}
