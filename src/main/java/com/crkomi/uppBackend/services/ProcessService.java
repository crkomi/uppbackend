package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.activity.services.MainProcessService;
import com.crkomi.uppBackend.activity.services.RegistrationService;
import com.crkomi.uppBackend.model.ResponsStartingProcessJson;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ProcessService {

    @Value("${processRegistrationResourcePath}")
    private String processRegistrationResourcePath;

    @Value("${mainProcessResourcePath}")
    private String mainProcessResourcePath;

    private ProcessEngine processEngine;

    public ProcessService(){

    }

    public void init(ProcessEngine processEngine){
        this.processEngine = processEngine;
        deploying();
    }

    private void deploying(){
        RepositoryService repositoryService = processEngine.getRepositoryService();
        for (Deployment d : repositoryService.createDeploymentQuery().list())
        {
            repositoryService.deleteDeployment(d.getId(), true);
        }
        repositoryService.createDeployment().addClasspathResource(processRegistrationResourcePath).deploy();
        repositoryService.createDeployment().addClasspathResource(mainProcessResourcePath).deploy();
    }

    public ResponsStartingProcessJson startRegistrationProcess(){
        String userUUID = UUID.randomUUID().toString();
        String userConfirmedUUID = UUID.randomUUID().toString();
        System.out.println("Process registration is starting");
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("registrationService", new RegistrationService());
        variables.put("initiator", userUUID);
        variables.put("confirmedInitiator", "user" + userConfirmedUUID);
        variables.put("enum_category", null);
        variables.put("distance", null);
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("registration", variables);
        return new ResponsStartingProcessJson(userUUID, processInstance.getId());
    }

    public ResponsStartingProcessJson startMainProcess(String initiator){
        RuntimeService runtimeService = processEngine.getRuntimeService();
        System.out.println("Main Process is starting");
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("mainProcessService", new MainProcessService());
        variables.put("initiator", initiator);
        variables.put("lastDateReceiptTendersString", "2200-2-12T22:32:00");
        variables.put("globalCounter", 0L);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("mainProcess", variables);
        return new ResponsStartingProcessJson(initiator, processInstance.getId());
    }

    public ProcessEngine getProcessEngine() {
        return processEngine;
    }

    public void setProcessEngine(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }
}