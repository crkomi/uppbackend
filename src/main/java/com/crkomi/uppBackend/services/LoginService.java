package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.model.LoginJson;
import com.crkomi.uppBackend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UserService userService;

    public LoginJson processingLogging(LoginJson loginJson){
        loginJson.setStatus(userService.loginProcessing(loginJson.getUsername(), loginJson.getPassword()));
        return loginJson;
    }

}
