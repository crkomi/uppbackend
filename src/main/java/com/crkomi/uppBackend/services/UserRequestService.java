package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.UserRequestEntity;
import com.crkomi.uppBackend.repositories.CompanyCategoryRepository;
import com.crkomi.uppBackend.repositories.UserRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRequestService {

    @Autowired
    private UserRequestRepository userRequestRepository;

    public UserRequestEntity findOne(Long id){
        return userRequestRepository.findOne(id);
    }

    public List<UserRequestEntity> findAll(){
        return userRequestRepository.findAll();
    }

    public UserRequestEntity save(UserRequestEntity userRequestEntity){
        return userRequestRepository.save(userRequestEntity);
    }

    public void delete(Long id){
        userRequestRepository.delete(id);
    }
}
