package com.crkomi.uppBackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class EmailService{

    @Autowired
    JavaMailSender javaMailSender;

    public void sendMail(String emailTo){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("upravljanjeposlovnimprocesima@gmail.com");
            emailSMM.setTo(emailTo);
            emailSMM.setSubject("Test");
            emailSMM.setText("Testing");
            javaMailSender.send(emailSMM);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }

    public void sendMail(String emailTo, String subject, String text){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("upravljanjeposlovnimprocesima@gmail.com");
            emailSMM.setTo(emailTo);
            emailSMM.setSubject(subject);
            emailSMM.setText(text);
            javaMailSender.send(emailSMM);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }


}

