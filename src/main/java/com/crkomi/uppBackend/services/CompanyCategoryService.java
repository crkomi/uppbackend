package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.repositories.CompanyCategoryRepository;
import com.crkomi.uppBackend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyCategoryService {

    @Autowired
    private CompanyCategoryRepository companyCategoryRepository;

    public CompanyCategoryEntity findOne(Long id){
        return companyCategoryRepository.findOne(id);
    }

    public List<CompanyCategoryEntity> findAll(){
        return companyCategoryRepository.findAll();
    }

    public CompanyCategoryEntity save(CompanyCategoryEntity companyCategoryEntity){
        return companyCategoryRepository.save(companyCategoryEntity);
    }

    public void delete(Long id){
        companyCategoryRepository.delete(id);
    }
}
