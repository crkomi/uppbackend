package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.repositories.LoanRepository;
import org.springframework.mail.javamail.JavaMailSender;

public class StaticResource {

    public static LoanRepository loanRepository;
    public static UserService userService;
    public static EmailService emailService;
    public static UserRequestService userRequestService;
    public static OfferService offerService;
    public static RatingService ratingService;

}
