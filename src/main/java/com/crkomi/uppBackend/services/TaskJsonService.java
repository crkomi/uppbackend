package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.OfferEntity;
import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.entities.UserRequestEntity;
import com.crkomi.uppBackend.model.EnumItemJson;
import com.crkomi.uppBackend.model.FormPropertyJson;
import com.crkomi.uppBackend.model.TaskJson;
import org.activiti.engine.FormService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaskJsonService {

    @Autowired
    private ProcessService processService;

    @Autowired
    private CompanyCategoryService companyCategoryService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private UserRequestService userRequestService;

    @Autowired
    private UserService userService;

    public TaskJsonService(){

    }

    public List<TaskJson> getAllTasks(String assigner){
        TaskService taskService = processService.getProcessEngine().getTaskService();
        ArrayList<TaskJson> retValue = new ArrayList<>();
        for(Task task : taskService.createTaskQuery().taskAssignee(assigner).list()){
            retValue.add(new TaskJson(task.getName(), task.getId()));
        }
        return retValue;
    }

    public List<FormPropertyJson> getFormPropeties(String taskId){
        ArrayList<FormPropertyJson> retValue = new ArrayList<>();
        FormService formService = processService.getProcessEngine().getFormService();
        int i=0;
        for(org.activiti.engine.form.FormProperty formPropertyInterface : formService.getTaskFormData(taskId).getFormProperties()){
            FormPropertyJson formPropertyJson = new FormPropertyJson();
            formPropertyJson.setLabel(formPropertyInterface.getName());
            formPropertyJson.setName(formPropertyInterface.getId());
            formPropertyJson.setValue(formPropertyInterface.getValue());
            System.out.println(i++ + " Possible: "+ formPropertyInterface.getType()) ;
            if(formPropertyInterface.getType().getName().equals("string")){
                formPropertyJson.setType("text");
            }else{
                formPropertyJson.setType(formPropertyInterface.getType().getName());
            }
            formPropertyJson.setReadable(formPropertyInterface.isReadable());
            formPropertyJson.setWritable(formPropertyInterface.isWritable());
            formPropertyJson.setRequired(formPropertyInterface.isRequired());
            if(formPropertyInterface.getType().getName().equals("enum")){
                HashMap<String, String> hashMapString =(HashMap<String, String>) formPropertyInterface.getType().getInformation("values");
                System.out.println("Values of enumaration: " + hashMapString.size());
                ArrayList<EnumItemJson> arrayListString = new ArrayList<>();
                for(String keyValue : hashMapString.keySet()){
                    arrayListString.add(new EnumItemJson(keyValue, hashMapString.get(keyValue)));
                }
                formPropertyJson.setValues(arrayListString);
            }
            if(formPropertyInterface.getId().startsWith("enum_")){
                formPropertyJson.setType("enum");
                ArrayList<EnumItemJson> arrayListEnumItems = new ArrayList<>();
                if(formPropertyInterface.getId().contains("enum_category")){
                    for(CompanyCategoryEntity companyCategoryEntity : companyCategoryService.findAll()){
                        arrayListEnumItems.add(new EnumItemJson(companyCategoryEntity.getActivitiName(), companyCategoryEntity.getName()));
                    }
                    formPropertyJson.setValues(arrayListEnumItems);
                }else if(formPropertyInterface.getId().contains("enum_rankedOffers")){
                    System.out.println("Imamo tim rankedOffers");
                    for(OfferEntity offerEntity : rankingOffers(Long.parseLong(formPropertyInterface.getValue()))){
                        arrayListEnumItems.add(new EnumItemJson(offerEntity.getId().toString(), offerEntity.getPrice() + "_" + offerEntity.getUsername() + "_" + offerEntity.getDateLimit()));
                    }
                    formPropertyJson.setValues(arrayListEnumItems);
                }
            }
            retValue.add(formPropertyJson);
        }
        return retValue;
    }

    public void postFormPropetiesOnTaskAndExecute(String taskId, List<FormPropertyJson> formPropetiesJson){
        Map<String, String> submitData = new HashMap<String, String>();
        for(FormPropertyJson formPropertyJson : formPropetiesJson){
            if(formPropertyJson.isWritable()){
                if(formPropertyJson.getType().equals("enum") && !formPropertyJson.getName().startsWith("enum_")){
                    for(int i=0;i<formPropertyJson.getValues().size(); i++){
                        if(formPropertyJson.getValues().get(i).getValue().equals(formPropertyJson.getValue())){
                            submitData.put(formPropertyJson.getName(), formPropertyJson.getValues().get(i).getName());
                            break;
                        }
                    }
                }else{
                    submitData.put(formPropertyJson.getName(), formPropertyJson.getValue());
                }
            }
        }
        FormService formService = processService.getProcessEngine().getFormService();
        formService.submitTaskFormData(taskId, submitData);
    }

    private List<OfferEntity> rankingOffers(Long requestId){
        System.out.println("Pozvana metoda rankingOffers");


        UserRequestEntity userRequestEntity = userRequestService.findOne(requestId);
        OfferEntity[] allOfferEntities = new OfferEntity[userRequestEntity.getOfferEntities().size()];
        int k=0;
        for(OfferEntity offerEntity : userRequestEntity.getOfferEntities()){
            allOfferEntities[k] = offerEntity;
            allOfferEntities[k].setUsername(userService.findOne(offerEntity.getUserId()).getUsername());
            k++;
            System.out.println("offerEntity.getId(): " + offerEntity.getId());
        }
        System.out.println("Size offerEntity: " + allOfferEntities.length);
        for(int i=0;i<allOfferEntities.length;i++){
            for(int j=i+1; j<allOfferEntities.length;j++){
                if(allOfferEntities[i].getPrice() > allOfferEntities[j].getPrice()){
                    OfferEntity temp = allOfferEntities[i];
                    allOfferEntities[i] = allOfferEntities[j];
                    allOfferEntities[j] = temp;
                }
            }
        }
        System.out.println("Nakon sorta allOfferEntities " + allOfferEntities.length);
        ArrayList<OfferEntity> retVal = new ArrayList<>();
        for(int i=0;i<allOfferEntities.length;i++){
            if(allOfferEntities[i].getState().equals(OfferService.DONE)){
                retVal.add(allOfferEntities[i]);
            }
        }
        System.out.println("Size retVal is: " + retVal.size());
        return retVal;
    }


}

