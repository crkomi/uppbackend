package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.OfferEntity;
import com.crkomi.uppBackend.repositories.CompanyCategoryRepository;
import com.crkomi.uppBackend.repositories.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferService {

    public static String CANCELED="canceled";
    public static String DONE="done";

    @Autowired
    private OfferRepository offerRepository;

    public OfferEntity findOne(Long id){
        return offerRepository.findOne(id);
    }

    public List<OfferEntity> findAll(){
        return offerRepository.findAll();
    }

    public OfferEntity save(OfferEntity offerEntity){
        return offerRepository.save(offerEntity);
    }

    public void delete(Long id){
        offerRepository.delete(id);
    }

}
