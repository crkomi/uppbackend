package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.LoanEntity;
import com.crkomi.uppBackend.repositories.LoanRepository;

import java.io.Serializable;

public class LoanService implements Serializable {

    public LoanEntity buildLoan(String clientsName, String clientsSurname, String clientsEmail, Long clientsIncome, Long requestedLoan, Boolean creditOk){
        LoanEntity loanEntity = new LoanEntity();
        loanEntity.setClientsEmail(clientsEmail);
        loanEntity.setClientsIncome(clientsIncome);
        loanEntity.setClientsName(clientsName);
        loanEntity.setClientsSurname(clientsSurname);
        loanEntity.setCreditOk(creditOk);
        loanEntity.setRequestedLoan(requestedLoan);
        System.out.println("Pozvala se metod preko servisa");
        return StaticResource.loanRepository.save(loanEntity);
    }
}
