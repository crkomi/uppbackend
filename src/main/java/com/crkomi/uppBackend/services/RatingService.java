package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.OfferEntity;
import com.crkomi.uppBackend.entities.RatingEntity;
import com.crkomi.uppBackend.repositories.OfferRepository;
import com.crkomi.uppBackend.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    public RatingEntity findOne(Long id){
        return ratingRepository.findOne(id);
    }

    public List<RatingEntity> findAll(){
        return ratingRepository.findAll();
    }

    public RatingEntity save(RatingEntity ratingEntity){
        return ratingRepository.save(ratingEntity);
    }

    public void delete(Long id){
        ratingRepository.delete(id);
    }

}
