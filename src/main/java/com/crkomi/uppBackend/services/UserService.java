package com.crkomi.uppBackend.services;

import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.repositories.UserRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity findOne(Long id){
        return userRepository.findOne(id);
    }

    public List<UserEntity> findAll(){
        return userRepository.findAll();
    }

    public UserEntity save(UserEntity userEntity){
        String address= userEntity.getAddress()  + ", " + userEntity.getCity() + ", Srbija";
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyAWnuJpEV40egdDJCjrptfQ43HWFPN6Rh4")
                .build();
        GeocodingResult[] results = null;
        try {
            results = GeocodingApi.geocode(context, address).await();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(results[0].geometry.location.lat));
        System.out.println(gson.toJson(results[0].geometry.location.lng));
        userEntity.setLatitude(gson.toJson(results[0].geometry.location.lat));
        userEntity.setLongitude(gson.toJson(results[0].geometry.location.lng));
        System.out.println("Latitude: " + userEntity.getLatitude());
        System.out.println("Longitude: " + userEntity.getLongitude());
        return userRepository.save(userEntity);
    }

    public void delete(Long id){
        userRepository.delete(id);
    }

    public boolean findByUsernameAndMail(String username, String email){
        if(userRepository.findByUsernameAndMail(username, email).size() == 0){
            return true;
        }else {
            return false;
        }
    }

    public boolean loginProcessing(String username, String password){
        return userRepository.findByUsernameAndPassword(username, password).size() == 1 && userRepository.findByUsernameAndPassword(username, password).get(0).getConfirmed();
    }

    public List<UserEntity> findByCompanyCategory(String companyCategory){
        return userRepository.findByCompanyCategory(companyCategory);
    }

    public List<UserEntity> findByUsername(String username){
        return userRepository.findByUsername(username);
    }

}
