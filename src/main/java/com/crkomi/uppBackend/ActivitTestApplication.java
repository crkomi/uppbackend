package com.crkomi.uppBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivitTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivitTestApplication.class, args);
	}
}
