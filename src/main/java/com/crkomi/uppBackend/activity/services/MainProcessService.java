package com.crkomi.uppBackend.activity.services;

import com.crkomi.uppBackend.entities.OfferEntity;
import com.crkomi.uppBackend.entities.RatingEntity;
import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.entities.UserRequestEntity;
import com.crkomi.uppBackend.services.OfferService;
import com.crkomi.uppBackend.services.StaticResource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainProcessService implements Serializable {

    public long saveRequest(String typeJob, String descriptionJob, long maxValue, Date lastDateReceiptTenders, long maxOffers, Date lastDateCompletionJob){
        UserRequestEntity userRequestEntity = new UserRequestEntity();
        userRequestEntity.setTypeJob(typeJob);
        userRequestEntity.setDescriptionJob(descriptionJob);
        userRequestEntity.setMaxValue(maxValue);
        userRequestEntity.setLastDateReceiptTenders(lastDateReceiptTenders);
        userRequestEntity.setMaxOffers(maxOffers);
        userRequestEntity.setLastDateCompletionJob(lastDateCompletionJob);
        userRequestEntity = StaticResource.userRequestService.save(userRequestEntity);
        return userRequestEntity.getId();
    }

    public Double getDistance(UserEntity userEntity1, UserEntity userEntity2){
        Integer R = 6371;
        Double latitude1 = Double.parseDouble(userEntity1.getLatitude());
        Double longitude1 = Double.parseDouble(userEntity1.getLongitude());
        Double latitude2 = Double.parseDouble(userEntity2.getLatitude());
        Double longitude2 = Double.parseDouble(userEntity2.getLongitude());

        Double dLat = deg2rad(latitude2-latitude1);  // deg2rad below
        Double dLon = deg2rad(longitude2-longitude1);


        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(latitude1)) * Math.cos(deg2rad(latitude2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);

        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double d = R * c; // Distance in km
        return d;

    }

    public Double deg2rad(Double deg) {

        return deg * (Math.PI/180);
    }

    public List<UserEntity> createingListOfCompanies(String typeJob, Long maxOffers, Long globalCounter, String username){
        List<UserEntity> userList = StaticResource.userService.findAll();
        UserEntity userEntity = StaticResource.userService.findByUsername(username).get(0);
        for(int i=userList.size()-1;i>=0;i--){
            if (userList.get(i).getCompanyCategory() != null) {
                if(!userList.get(i).getCompanyCategory().contains(typeJob)){
                    userList.remove(i);
                    continue;
                }
                if(userList.get(i).getDistance() < getDistance(userEntity, userList.get(i))){
                    userList.remove(i);
                }
            }else {
                userList.remove(i);
            }
        }
        System.err.println("CATEGORY : "+typeJob);
        System.err.println("SIZE : "+userList.size());
        System.err.println("MAX : "+maxOffers);
        System.err.println("globalCounter : "+globalCounter);
        ArrayList<UserEntity> retVal = new ArrayList<>();
        int counter = Integer.parseInt(globalCounter.toString());
        int max = Integer.parseInt(maxOffers.toString());

        int i = counter*max -1;
        for(UserEntity user : userList){
            if(userList.indexOf(user) <= i){
                continue;
            }
            if(retVal.size() == maxOffers){
                break;
            }
            retVal.add(user);
        }
        System.err.println("RET SIZE : "+retVal.size());
        return retVal;

    }

    public boolean checkValidityOfMailAndUsername(String username, String email){
        return StaticResource.userService.findByUsernameAndMail(username, email);
    }

    private void sendNotification(String emailTo, String subject, String text){
        StaticResource.emailService.sendMail(emailTo, subject, text);
    }

    public void sendNotificationCompanyDontExist(String initiator){
        UserEntity userEntity = StaticResource.userService.findByUsername(initiator).get(0);
        sendNotification(userEntity.getEmail(), "Notification", "Their is no company for your request");
    }

    public String getSelectedUsername(String selectedRow){
        return selectedRow.split("_")[1];
    }

    public void sendNotificationDoYouWantProcessFurther(String initiator){
        UserEntity userEntity = StaticResource.userService.findByUsername(initiator).get(0);
        sendNotification(userEntity.getEmail(), "Notification", "Do you want to process further?");
    }

    public void sendNotificationToAgentOfCompany(String initiatorMail){
        sendNotification(initiatorMail, "Notification", "Please accept your offers.");
    }

    public long calculateCurrentRankedOffer(long requestId, long price, Date timeLimit){
        UserRequestEntity userRequestEntity = StaticResource.userRequestService.findOne(requestId);
        long retVal = 1;
        System.out.println("Number of offers is: " + userRequestEntity.getOfferEntities());
        for(OfferEntity offerEntity : userRequestEntity.getOfferEntities()){
            if(offerEntity.getState().equals(OfferService.DONE)){
                if(offerEntity.getPrice()< price){
                    retVal++;
                }
            }
        }
        return retVal;
    }

    public void saveOfferDone(long requestId, long price, Date timeLimit, Long userId){
        OfferEntity offerEntity = new OfferEntity();
        offerEntity.setDateLimit(timeLimit);
        offerEntity.setPrice(price);
        offerEntity.setUserId(userId);
        offerEntity.setState(OfferService.DONE);
        UserRequestEntity userRequestEntity = StaticResource.userRequestService.findOne(requestId);
        userRequestEntity.getOfferEntities().add(offerEntity);
        offerEntity.setUserRequestEntity(StaticResource.userRequestService.findOne(requestId));
        userRequestEntity = StaticResource.userRequestService.save(userRequestEntity);
        //offerEntity = StaticResource.offerService.save(offerEntity);
    }

    public void saveOfferCancel(long requestId, Long userId){
        OfferEntity offerEntity = new OfferEntity();
        offerEntity.setUserId(userId);
        offerEntity.setState(OfferService.CANCELED);
        offerEntity.setUserRequestEntity(StaticResource.userRequestService.findOne(requestId));
        UserRequestEntity userRequestEntity = StaticResource.userRequestService.findOne(requestId);
        userRequestEntity.getOfferEntities().add(offerEntity);
        userRequestEntity = StaticResource.userRequestService.save(userRequestEntity);
        //offerEntity = StaticResource.offerService.save(offerEntity);
    }

    public String convertTime(String lastDateReceiptTenders, String time){
        System.out.println("Datum kada se prebaci u string je: " + lastDateReceiptTenders.toString());
        String retVal = lastDateReceiptTenders.toString().split("T")[0];
        retVal += "T" + time;
        return  retVal;
    }

    public List<OfferEntity> analyzingAllOffers(Long requestId){
        ArrayList<OfferEntity> retVal = new ArrayList<>();
        System.out.println("originalna velicnia liste iz analyzingAllOffers je: " + StaticResource.userRequestService.findOne(requestId).getOfferEntities().size());
        System.out.println();
        for(OfferEntity offerEntity : StaticResource.userRequestService.findOne(requestId).getOfferEntities()){
            if(offerEntity.getState().equals(OfferService.DONE)){
                retVal.add(offerEntity);
            }
        }
        System.out.println("velicnia liste iz analyzingAllOffers je: " + retVal.size());
        return retVal;
    }

    public void sendNotificationTheInsufficientCompanyOffers(String initiator){
        UserEntity userEntity = StaticResource.userService.findByUsername(initiator).get(0);
        sendNotification(userEntity.getEmail(), "" +
                "Notification", "The insufficient company offers");
    }

    public void sendNotificationOffersDoesntExist(String initiator){
        UserEntity userEntity = StaticResource.userService.findByUsername(initiator).get(0);
        sendNotification(userEntity.getEmail(), "Notification", "Offers dosn't exist.");
    }

    public List<UserEntity> chooseAnotherCompanies(Long requestId, List<UserEntity> oldList){
        ArrayList<UserEntity> retVal = new ArrayList<>();
        for(int i=0;i<oldList.size();i++){
            retVal.add(oldList.get(i));
        }
        UserRequestEntity userRequestEntity = StaticResource.userRequestService.findOne(requestId);
        for(UserEntity userEntity : oldList){
            for(OfferEntity offerEntity : userRequestEntity.getOfferEntities()){
                if(userEntity.getId() == offerEntity.getUserId()){
                    retVal.remove(userEntity);
                }
            }
        }
        return retVal;
    }

    public String rankingOffers(Long requestId){
        UserRequestEntity userRequestEntity = StaticResource.userRequestService.findOne(requestId);
        List<OfferEntity> allOfferEntities = userRequestEntity.getOfferEntities();
        return requestId.toString();
    }

    public void sendNotificationConfirmationAcceptanceSent(String username){
        UserEntity userEntity = StaticResource.userService.findByUsername(username).get(0);
        sendNotification(userEntity.getEmail(), "Notification", "Confirmation of acceptance is sent");
    }

    public void saveRating(String userNameClient, Long userClientRating, String userNameCompany, Long userCompanyRating){
        RatingEntity ratingEntity1 = new RatingEntity();
        ratingEntity1.setUsernameOne(userNameClient);
        ratingEntity1.setUsernameTwo(userNameCompany);
        ratingEntity1.setRation(userClientRating);
        ratingEntity1 = StaticResource.ratingService.save(ratingEntity1);
        RatingEntity ratingEntity2 = new RatingEntity();
        ratingEntity2.setUsernameOne(userNameCompany);
        ratingEntity2.setUsernameTwo(userNameClient);
        ratingEntity2.setRation(userCompanyRating);
        ratingEntity2 = StaticResource.ratingService.save(ratingEntity2);
    }

    public long restart(long globalCounter){
        globalCounter = globalCounter + 1;
        return globalCounter;
    }

    public long defaultLong(){
        return 0L;
    }

    public Date defaultDate(){
        return new Date();
    }
}
