package com.crkomi.uppBackend.activity.services;

import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.services.StaticResource;
import com.crkomi.uppBackend.services.UserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RegistrationService implements Serializable {

    public Long saveUser(String name, String email, String username, String password, String address, String city, String postalCode, String companyCategory, Long distance){
        UserEntity userEntity = new UserEntity(name,email, username, password, address, city, postalCode, companyCategory, false, distance);
        userEntity = StaticResource.userService.save(userEntity);
        return userEntity.getId();
    }

    public boolean checkValidityOfMailAndUsername(String username, String email){
        return StaticResource.userService.findByUsernameAndMail(username, email);
    }

    public void removeTemporaryData(Long id){
        StaticResource.userService.delete(id);
    }

    public void finishRegistration(Long id){
        UserEntity userEntity = StaticResource.userService.findOne(id);
        userEntity.setConfirmed(true);
        StaticResource.userService.save(userEntity);
    }

    public void sendNotification(String emailTo, String assigner){
        StaticResource.emailService.sendMail(emailTo, "verification mail","You have to verify your account, by clicking: http://localhost:4200/tasks?user=" + assigner);
    }

    public String addingCategory(String enum_categoryTemp, String enum_category){
        if(enum_category == null){
            enum_category = enum_categoryTemp;
            return enum_category;
        }
        String[] currentCategories = enum_category.split("_");

        for(int i=0;i<currentCategories.length;i++){
            if(currentCategories[i].equals(enum_categoryTemp)){
                return enum_category;
            }
        }
        enum_category += "_" + enum_categoryTemp;
        return enum_category;
    }

}
