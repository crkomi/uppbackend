package com.crkomi.uppBackend.controllers;

import com.crkomi.uppBackend.model.FormPropertyJson;
import com.crkomi.uppBackend.model.TaskJson;
import com.crkomi.uppBackend.services.TaskJsonService;
import org.activiti.bpmn.model.FormProperty;
import org.activiti.engine.ProcessEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    private TaskJsonService taskJsonService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<TaskJson>> getUserTasks(@PathVariable("userId") String userId){
        return new ResponseEntity<List<TaskJson>>(taskJsonService.getAllTasks(userId), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/form/{taskId}", method = RequestMethod.GET)
    public ResponseEntity<List<FormPropertyJson>> getFormProperties(@PathVariable("taskId") String taskId){
        return new ResponseEntity<List<FormPropertyJson>>(taskJsonService.getFormPropeties(taskId), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/form/{taskId}", method = RequestMethod.POST)
    public ResponseEntity<List<FormPropertyJson>> postFormPropetiesOnTaskAndExecute(@PathVariable("taskId") String taskId, @RequestBody List<FormPropertyJson> formPropeties) {
        if (formPropeties == null) {
            return new ResponseEntity<List<FormPropertyJson>>(HttpStatus.BAD_REQUEST);
        }
        taskJsonService.postFormPropetiesOnTaskAndExecute(taskId, formPropeties);

        return new ResponseEntity<List<FormPropertyJson>>(formPropeties, HttpStatus.OK);
    }

}
