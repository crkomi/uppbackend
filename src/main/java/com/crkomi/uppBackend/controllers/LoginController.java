package com.crkomi.uppBackend.controllers;

import com.crkomi.uppBackend.model.FormPropertyJson;
import com.crkomi.uppBackend.model.LoginJson;
import com.crkomi.uppBackend.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<LoginJson> postLoginJson(@RequestBody LoginJson loginJson) {
        if (loginJson == null) {
            return new ResponseEntity<LoginJson>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<LoginJson>(loginService.processingLogging(loginJson), HttpStatus.OK);
    }

}
