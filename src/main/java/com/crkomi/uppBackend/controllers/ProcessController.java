package com.crkomi.uppBackend.controllers;

import com.crkomi.uppBackend.model.FormPropertyJson;
import com.crkomi.uppBackend.model.ResponsStartingProcessJson;
import com.crkomi.uppBackend.services.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/process")
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ResponseEntity<ResponsStartingProcessJson> startRegistrationProcess(){
        return new ResponseEntity<ResponsStartingProcessJson>(processService.startRegistrationProcess(), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/mainProcess", method = RequestMethod.POST)
    public ResponseEntity<ResponsStartingProcessJson> postStartMainProcess(@RequestBody String initiator) {
        if (initiator == null) {
            return new ResponseEntity<ResponsStartingProcessJson>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<ResponsStartingProcessJson>(processService.startMainProcess(initiator), HttpStatus.OK);
    }

}
