package com.crkomi.uppBackend.controllers;

import com.crkomi.uppBackend.activity.services.RegistrationService;
import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.repositories.LoanRepository;
import com.crkomi.uppBackend.services.*;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import org.activiti.engine.*;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class InitController implements CommandLineRunner {

    private static final String groupsPath ="./src/main/resources/properties/groups.yml";
    private static final String usersPath ="./src/main/resources/properties/users.yml";

    @Autowired
    private ProcessService processService;

    @Autowired
    private CompanyCategoryService companyCategoryService;

    @Autowired
    private UserService userService;
    @Autowired
    public InitController(LoanRepository loanRepository, UserService userService, EmailService emailService, OfferService offerService, UserRequestService userRequestService, RatingService ratingService){
        StaticResource.loanRepository = loanRepository;
        StaticResource.userService = userService;
        StaticResource.emailService = emailService;
        StaticResource.userRequestService = userRequestService;
        StaticResource.offerService = offerService;
        StaticResource.ratingService = ratingService;
    }

    @Override
    public void run(String... args) throws Exception {
        processService.init(ProcessEngines.getDefaultProcessEngine());

        CompanyCategoryEntity companyCategoryEntity1 = new CompanyCategoryEntity("IT", "it");
        CompanyCategoryEntity companyCategoryEntity2 = new CompanyCategoryEntity("Building", "building");
        CompanyCategoryEntity companyCategoryEntity3 = new CompanyCategoryEntity("School", "school");
        CompanyCategoryEntity companyCategoryEntity4 = new CompanyCategoryEntity("Fix", "fix");
        companyCategoryEntity1 = companyCategoryService.save(companyCategoryEntity1);
        companyCategoryEntity2 = companyCategoryService.save(companyCategoryEntity2);
        companyCategoryEntity3 = companyCategoryService.save(companyCategoryEntity3);
        companyCategoryEntity4 = companyCategoryService.save(companyCategoryEntity4);

        UserEntity userEntity = new UserEntity();
        userEntity.setAddress("Dunavaska");
        userEntity.setCity("Backa Topla");
        userEntity.setConfirmed(true);
        userEntity.setCompanyCategory(null);
        userEntity.setEmail("mirkoodalovic01@gmail.com");
        userEntity.setPassword("crkomi");
        userEntity.setUsername("crkomi");
        userEntity.setName("Mirko");
        userEntity.setPostalCode("24300");
        userEntity = userService.save(userEntity);

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setAddress("Dunavaska");
        userEntity1.setCity("Backa Topla");
        userEntity1.setConfirmed(true);
        userEntity1.setCompanyCategory("Building");
        userEntity1.setEmail("bakirniksic@yahoo.com");
        userEntity1.setPassword("crkomi");
        userEntity1.setUsername("aleksandar");
        userEntity1.setName("Mirko");
        userEntity1.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity1 = userService.save(userEntity1);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setAddress("Segedinski put");
        userEntity2.setCity("Subotica");
        userEntity2.setConfirmed(true);
        userEntity2.setCompanyCategory("Building");
        userEntity2.setEmail("bakirniksic01@yahoo.com");
        userEntity2.setPassword("crkomi");
        userEntity2.setUsername("bakir");
        userEntity2.setName("Mirko");
        userEntity2.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity2 = userService.save(userEntity2);

        UserEntity userEntity3 = new UserEntity();
        userEntity3.setAddress("Dunavaska");
        userEntity3.setCity("Backa Topla");
        userEntity3.setConfirmed(true);
        userEntity3.setCompanyCategory("Building");
        userEntity3.setEmail("mirkoodalovic011@gmail.com");
        userEntity3.setPassword("crkomi");
        userEntity3.setUsername("crkomii");
        userEntity3.setName("Mirko");
        userEntity3.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity3 = userService.save(userEntity3);

        UserEntity userEntity4 = new UserEntity();
        userEntity4.setAddress("Dunavaska");
        userEntity4.setCity("Backa Topla");
        userEntity4.setConfirmed(true);
        userEntity4.setCompanyCategory("Building");
        userEntity4.setEmail("mirkoodalovic0114@gmail.com");
        userEntity4.setPassword("crkomi");
        userEntity4.setUsername("crkomi4");
        userEntity4.setName("Mirko");
        userEntity4.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity4 = userService.save(userEntity4);

        UserEntity userEntity5 = new UserEntity();
        userEntity5.setAddress("Dunavaska");
        userEntity5.setCity("Backa Topla");
        userEntity5.setConfirmed(true);
        userEntity5.setCompanyCategory("Building");
        userEntity5.setEmail("mirkoodalovic011@gmail.com");
        userEntity5.setPassword("crkomi");
        userEntity5.setUsername("crkomi5");
        userEntity5.setName("Mirko");
        userEntity5.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity5 = userService.save(userEntity5);

        UserEntity userEntity6 = new UserEntity();
        userEntity6.setAddress("Dunavaska");
        userEntity6.setCity("Backa Topla");
        userEntity6.setConfirmed(true);
        userEntity6.setCompanyCategory("Building");
        userEntity6.setEmail("mirkoodalovic011@gmail.com");
        userEntity6.setPassword("crkomi");
        userEntity6.setUsername("crkomi6");
        userEntity6.setName("Mirko");
        userEntity6.setPostalCode("24300");
        userEntity1.setDistance(50);
        userEntity6 = userService.save(userEntity6);
        /*
        RepositoryService repositoryService = processEngine.getRepositoryService();
        for (Deployment d : repositoryService.createDeploymentQuery().list())
        {
            repositoryService.deleteDeployment(d.getId(), true);
        }

        repositoryService.createDeployment().addClasspathResource(filename).deploy();
       // repositoryService.createDeployment().addClasspathResource(filename).deploy();
        */
        /*



        Map<String, String> companyCategoriesMap = new HashMap<String, String>();
        for(CompanyCategoryEntity companyCategoryEntity : companyCategoryService.findAll()){
            System.out.println("id :" + companyCategoryEntity.getActivitiName() + "name" + companyCategoryEntity.getName());
            companyCategoriesMap.put(companyCategoryEntity.getActivitiName(), companyCategoryEntity.getName());
        }
        EnumFormType companyCategoriesEnum = new EnumFormType(companyCategoriesMap);


        String userUUID = UUID.randomUUID().toString();
        String userConfirmedUUID = UUID.randomUUID().toString();

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("companyCategories", companyCategoriesEnum);
        variables.put("registrationService", new RegistrationService());
        variables.put("initiator", "user" + userUUID);
        variables.put("confirmedInitiator", "user" + userConfirmedUUID);
        variables.put("category", null);
        System.out.println("Random user UUID je: " + userUUID);
        RuntimeService runtimeService = processService.getProcessEngine().getRuntimeService();

        System.out.println("Pre startovanja procesa imam aktivnih procesa: " + runtimeService.createProcessInstanceQuery().count());
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("registration", variables);
        System.out.println("Posle startovanja procesa imam aktivnih procesa: " + runtimeService.createProcessInstanceQuery().count());
        TaskService taskService = processService.getProcessEngine().getTaskService();
        List<Task> userTasks = taskService.createTaskQuery().taskAssignee("user" + userUUID).list();
        System.out.println("User je dodoljeno taskova: " + userTasks.size());
        Map<String, String> submitData = new HashMap<String, String>();
        submitData.put("name", "Mirko");
        submitData.put("username","Odalovic");
        submitData.put("email","mirkoodalovic01@gmail.com");
        submitData.put("password","12345678");
        submitData.put("address","Dunavska 1/1");
        submitData.put("city","Backa Topola");
        submitData.put("type", "person");
        submitData.put("postalCode", "24300");
        FormService formService = processService.getProcessEngine().getFormService();
        formService.submitTaskFormData(userTasks.get(0).getId(), submitData);

    //    Map<String, String> submitDataa = new HashMap<String, String>();
     //   submitDataa.put("category", "sport");
  //      formService = processService.getProcessEngine().getFormService();
  //      userTasks = taskService.createTaskQuery().taskAssignee("user" + userUUID).list();
//        formService.submitTaskFormData(userTasks.get(0).getId(), submitDataa);

        List<Task> userConfirmedTasks = taskService.createTaskQuery().taskAssignee("user" + userConfirmedUUID).list();
        System.out.println("UserConfirmed je dodoljeno taskova: " + userConfirmedTasks.size());
        formService.submitTaskFormData(userConfirmedTasks.get(0).getId(), new HashMap<String, String>());
        System.out.println("Nakin svih obavljenih zadataka, imam aktivnih procesa: " + runtimeService.createProcessInstanceQuery().count());
        */
        //List<FormProperty> formProperties = ;
        //FormProperty formProperty = new FormProperty(formService.getTaskFormData(userTasks.get(0).getId()).getFormProperties())
        /*
        System.out.println("Ukupan broj deployment-a: " + repositoryService.createDeploymentQuery().count());

        IdentityService identityService = processEngine.getIdentityService();

        long groupsNum =  identityService.createGroupQuery().count();
        if (groupsNum == 0)
            initGroupsYml(identityService);
        long usersNum = identityService.createUserQuery().count();
        if (usersNum == 0)
            initUsersYml(identityService);
        System.out.println("Broj grupa: " + identityService.createGroupQuery().count());
        System.out.println("Broj korisnika: " + identityService.createUserQuery().count());
        System.out.println("Broj korisnika u grupi bankar:  " + identityService.createUserQuery().memberOfGroup("bankar").count());

        x`gradnja","Gradnja");

        EnumFormType companyCategoriesEnum = new EnumFormType(companyCategoriesItems);

        Map<String, Object> variables = new HashMap<String, Object>();

        variables.put("companyCategories", companyCategoriesEnum);

        RuntimeService runtimeService = processEngine.getRuntimeService();
        TaskService taskService = processEngine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("bankar").list();
        System.out.println("Bankar pre startovanja procesa ima: " + tasks.size() + " taskova");

        //TaskService taskService = processEngine.getTaskService();
        //List<Task> tasks = taskService.createTaskQuery().taskAssignee("kermit").list();
        //tasks = taskService.createTaskQuery().taskCandidateUser("kermit").list();




        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("loanRequest", variables);
        System.out.println("Bankar of process instances: " +
                runtimeService.createProcessInstanceQuery().count());

        taskService = processEngine.getTaskService();
        tasks = taskService.createTaskQuery().taskCandidateGroup("bankar").list();


        System.out.println("Bankar nakon startovanja procesa ima: " + tasks.size() + " taskova");

        System.out.println("Bankar odobrava task");
        Task task = tasks.get(0);
        Map<String, Object> taskVariables = new HashMap<String, Object>();
        taskVariables.put("odobreno", "ne");
        tasks = taskService.createTaskQuery().taskCandidateGroup("bankar").list();
        taskService.complete(task.getId(), taskVariables);
        System.out.println("Nakon odobravanja taska bankar ima: " + tasks.size() + " taskova");
        System.out.println("Trenutno imam aktivnih procesa: " + runtimeService.createProcessInstanceQuery().count());
        tasks = taskService.createTaskQuery().taskCandidateGroup("bankar").list();
        //taskService.complete(tasks.get(0).getId());
        System.out.println("Trenutno imam aktivnih procesa nakon izvrsenja bankarevog taska: " + runtimeService.createProcessInstanceQuery().count());
        */
    }

    @SuppressWarnings("rawtypes")
    private static void initGroupsYml(IdentityService identityService){
        YamlReader reader = null;
        Map map;
        Group newGroup;
        try {
            reader = new YamlReader(new FileReader(groupsPath));
            while (true) {
                map = (Map) reader.read();
                if (map == null)
                    break;
                newGroup = identityService.newGroup((String) map.get("id"));
                newGroup.setName((String) map.get("name"));
                newGroup.setType((String) map.get("type"));
                identityService.saveGroup(newGroup);
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (YamlException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static void initUsersYml(IdentityService identityService){
        YamlReader reader = null;
        Map map;
        User newUser;
        try {
            reader = new YamlReader(new FileReader(usersPath));
            while (true) {
                map = (Map) reader.read();
                if (map == null)
                    break;
                newUser = identityService.newUser((String) map.get("id"));
                newUser.setFirstName((String) map.get("firstName"));
                newUser.setLastName((String) map.get("lastName"));
                newUser.setEmail((String) map.get("email"));
                newUser.setPassword((String) map.get("password"));
                identityService.saveUser(newUser);

                for (HashMap recordMap : (List<HashMap>) map.get("groups"))
                    identityService.createMembership(newUser.getId(),(String) recordMap.get("id"));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (YamlException e) {
            e.printStackTrace();
        }
    }

}
