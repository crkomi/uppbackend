package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.UserEntity;
import com.crkomi.uppBackend.entities.UserRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRequestRepository extends JpaRepository<UserRequestEntity, Long> {
}
