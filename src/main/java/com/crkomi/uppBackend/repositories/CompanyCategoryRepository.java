package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.CompanyCategoryEntity;
import com.crkomi.uppBackend.entities.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface CompanyCategoryRepository extends JpaRepository<CompanyCategoryEntity, Long>{
}
