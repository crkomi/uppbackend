package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.LoanEntity;
import com.crkomi.uppBackend.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from UserEntity u where u.username = :username or u.email = :email")
    public List<UserEntity> findByUsernameAndMail(@Param("username") String username, @Param("email") String email);

    @Query("select u from UserEntity u where u.username = :username and u.password = :password")
    public List<UserEntity> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query("select u from UserEntity u where u.companyCategory = :companyCategory")
    public List<UserEntity> findByCompanyCategory(@Param("companyCategory") String companyCategory);

    @Query("select u from UserEntity u where u.username = :username")
    public List<UserEntity> findByUsername(@Param("username") String username);
}
