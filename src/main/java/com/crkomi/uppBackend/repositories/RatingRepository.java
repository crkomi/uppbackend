package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.OfferEntity;
import com.crkomi.uppBackend.entities.RatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {
}
