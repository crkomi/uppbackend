package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface LoanRepository extends JpaRepository<LoanEntity, Long>, Serializable {
}
