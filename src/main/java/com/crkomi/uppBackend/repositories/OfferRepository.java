package com.crkomi.uppBackend.repositories;

import com.crkomi.uppBackend.entities.OfferEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferRepository extends JpaRepository<OfferEntity, Long> {
}
