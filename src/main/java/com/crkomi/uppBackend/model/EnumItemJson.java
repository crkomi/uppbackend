package com.crkomi.uppBackend.model;

public class EnumItemJson {

    private String name;
    private String value;

    public EnumItemJson(){

    }

    public EnumItemJson(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
