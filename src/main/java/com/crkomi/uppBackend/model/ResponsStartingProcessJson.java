package com.crkomi.uppBackend.model;

public class ResponsStartingProcessJson {

    private String currentUser;
    private String idProcess;

    public ResponsStartingProcessJson(){

    }

    public ResponsStartingProcessJson(String currentUser, String idProcess) {
        this.currentUser = currentUser;
        this.idProcess = idProcess;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }
}
